# Sistem Informasi Pemerintah Daerah (SIPD) - Prototype
SIPD microservices application in isolated Docker containers.

## Getting Started
These instructions will get you a copy of the project up and running on isolated docker container and on your local machine.

### Prerequisites
Prequisites package:
* [Docker](https://www.docker.com/get-started) Open platform for developing, shipping, and running applications.

### Running In Docker Container
1. Check whether the docker is installed and running on your machine or not by this command:
    ```bash
    # Information about your Docker Client and Server versions.
    $ docker -v

    # Check if a docker is running
    $ docker ps
    ```

2. Rename `.env.example` to `.env`.
    ```bash
    $ cp .env.example .env
    ```
    You can modify its content for custom configuration.

3. Run project by this command:
    ```bash
    $ docker-compose up -d --build 
    ```

#### API Documentation
Auth service api documentation page available at:

[http://127.0.0.1:8080/swagger/index.html](http://localhost:8080/swagger/index.html)

Profile service api documentation page available at:

[http://127.0.0.1:8081/swagger/index.html](http://localhost:8081/swagger/index.html)

### Stoping All Service
Stop all service by this command:
```bash
$ docker-compose down -v
```

## Built With
* [Go](https://golang.org/) - Go Programming Languange
* [Go Modules](https://github.com/golang/go/wiki/Modules) - Go Dependency Management System
* [Make](https://www.gnu.org/software/make/) - GNU Make Automated Execution
* [Docker](https://www.docker.com/) - Application Containerization

## Usefull Doc.
* [https://jigarius.com/blog/multiple-git-remote-repositories](https://jigarius.com/blog/multiple-git-remote-repositories) - Multiple git remote
* [https://upcloud.com/community/tutorials/deploy-kubernetes-dashboard](https://upcloud.com/community/tutorials/deploy-kubernetes-dashboard) - How to deploy Kubernetes Dashboard (Article)
* [https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard) - How to deploy Kubernetes Dashboard (Official Doc.)

## Authors
* **Fajar Rizky** - *Initial Work* - [cooljar](https://github.com/cooljar)

## More
--------