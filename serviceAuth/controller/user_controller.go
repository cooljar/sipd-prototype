package controller

import (
	"crypto/md5"
	"encoding/hex"
	"github.com/jackc/pgx/v4"
	"serviceAuth/models"
	"serviceAuth/models/errors"
	"serviceAuth/models/form"
	"serviceAuth/repository"
	"strings"
	"time"
)

type UserController struct {
	Repo *repository.UserRepository
	contextTimeout time.Duration
}

func NewUserController(repo *repository.UserRepository, timeout time.Duration) *UserController {
	return &UserController{
		Repo:           repo,
		contextTimeout: timeout,
	}
}

func (u *UserController) Signup(sf form.SignupForm) (err error) {
	err = u.Repo.Signup(sf)

	return
}

func (u *UserController) Login(lf form.LoginForm) (user models.User, err error) {
	user, err = u.Repo.Login(lf)
	if err != nil {
		if err == pgx.ErrNoRows {
			err = errors.DataValidationError{Field: "password", Message: "invalid username or password"}
		}
		return
	}

	if validatePassword(lf.Password, user.LoginPasswd.(string)) == false {
		err = errors.DataValidationError{Field: "password", Message: "invalid username or password"}
		return
	}

	return
}

func validatePassword(content, encrypted string) bool {
	return strings.EqualFold(encodePasswd(content), encrypted)
}

func encodePasswd(data string) string {
	h := md5.New()
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
}
