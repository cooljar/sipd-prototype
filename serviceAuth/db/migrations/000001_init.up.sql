-- Add UUID extension
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Set timezone
-- For more information, please visit:
-- https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
-- SET TIMEZONE="UTC";

-- Create user table
create table "user"
(
    id UUID DEFAULT uuid_generate_v4 () PRIMARY KEY,
    email                varchar(255)       not null,
    phone                varchar(25)        not null,
    username             varchar(255)       not null,
    password_hash        varchar(255)       not null,
    password_reset_token varchar(255)       not null constraint user_password_reset_token_key unique,
    verification_token   varchar(255)       not null,
    status               smallint default 0 not null,
    created_at           integer  default 0 not null,
    updated_at           integer  default 0 not null
);
comment on column "user".status is '0=Inactive, 1=Active, 2=Deleted';

create unique index user_index_email on "user" (email);
create unique index user_index_username on "user" (username);
create index user_index_ustatus on "user" (username, status);
