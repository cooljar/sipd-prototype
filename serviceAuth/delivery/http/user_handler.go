package http

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"serviceAuth/controller"
	"serviceAuth/models/form"
	"serviceAuth/models/http_response"
	"serviceAuth/utils"
)

type UserHandler struct {
	Controller *controller.UserController
	Validate *validator.Validate
}

func NewUserHandler(rApi, rapiPrivate fiber.Router, validator *validator.Validate, controller *controller.UserController )  {
	handler := &UserHandler{
		Controller: controller,
		Validate:   validator,
	}

	rUser := rApi.Group("/user")
	rUser.Post("/signup", handler.Signup)
	rUser.Post("/login", handler.Login)

	//rUserPrivate := rapiPrivate.Group("/user")
	//rUserPrivate.Get("/refresh-token", handler.RefreshToken)
}

// Signup func for signup.
// @Summary user signup
// @Description Signup to get JWT token.
// @Tags User
// @Accept mpfd
// @Param email formData string true "Email"
// @Param phone formData string true "Phone"
// @Param username formData string true "Username"
// @Param password formData string true "Password"
// @Param password_repeat formData string true "Confirm Password"
// @Param captcha formData string true "Captcha"
// @Produce json
// @Param signup form body form.SignupForm true "Fill form"
// @success 200 {object} http_response.JSONResult{data=string} "Registration Success"
// @Failure 400 {object} http_response.HTTPError
// @Failure 404 {object} http_response.HTTPError
// @Failure 422 {array} http_response.HTTPError
// @Failure 500 {object} http_response.HTTPError
// @Router /user/signup [post]
func (uh *UserHandler) Signup(c *fiber.Ctx) error {
	var signupResponse http_response.JSONResult

	signupForm := form.SignupForm{
		Username:       c.FormValue("username"),
		Password:       c.FormValue("password"),
		PasswordRepeat: c.FormValue("password_repeat"),
	}

	// Validate form input
	err := uh.Validate.Struct(signupForm)
	if err != nil {
		return http_response.NewHttpError(c, err)
	}

	err = uh.Controller.Signup(signupForm)
	if err != nil {
		return http_response.NewHttpError(c, err)
	}

	signupResponse.Message = "Registration Success"
	signupResponse.Data = "Registration Success, continue to login"
	return c.JSON(signupResponse)
}

// Login func for login.
// @Summary user login
// @Description Login to get JWT token.
// @Tags User
// @Accept mpfd
// @Param username formData string true "Username"
// @Param password formData string true "Password"
// @Produce json
// @Success 200 {object} http_response.JSONResult{data=string} "Login Success, JWT Token provided"
// @Failure 400 {object} http_response.HTTPError
// @Failure 422 {object} []http_response.HTTPError
// @Failure 404 {object} http_response.HTTPError
// @Failure 500 {object} http_response.HTTPError
// @Router /user/login [post]
func (uh *UserHandler) Login(c *fiber.Ctx) error {
	loginForm := form.LoginForm{
		Username: c.FormValue("username"),
		Password: c.FormValue("password"),
	}

	// Validate form input
	err := uh.Validate.Struct(loginForm)
	if err != nil {
		return http_response.NewHttpError(c, err)
	}

	user, err := uh.Controller.Login(loginForm)
	if err != nil {
		return http_response.NewHttpError(c, err)
	}

	var loginResponse http_response.JSONResult
	loginResponse.Message = "Login Success, JWT Token provided"

	// Generate a new Access token.
	loginResponse.Data, err = utils.GenerateNewAccessToken(&user)
	if err != nil {
		return http_response.NewHttpError(c, err)
	}

	return c.JSON(loginResponse)
}