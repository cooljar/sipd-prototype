module serviceAuth

go 1.16

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/arsmn/fiber-swagger/v2 v2.17.0
	github.com/go-playground/validator/v10 v10.9.0
	github.com/gofiber/fiber/v2 v2.18.0
	github.com/gofiber/jwt/v2 v2.2.7
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/google/uuid v1.1.2
	github.com/jackc/pgx/v4 v4.13.0
	github.com/swaggo/swag v1.7.1
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
