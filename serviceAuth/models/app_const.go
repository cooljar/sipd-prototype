package models

const (
	ConstUserStatusInnactive = 0
	ConstUserStatusActive = 1
	ConstUserStatusDeleted = 2
)
