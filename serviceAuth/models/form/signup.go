package form

type SignupForm struct {
	Username        string `json:"username" validate:"required,lowercase,alphanumunicode"`
	Password        string `json:"password" validate:"required"`
	PasswordRepeat  string `json:"password_repeat" validate:"required,eqfield=Password"`
}
