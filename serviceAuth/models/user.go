package models

import (
	"encoding/json"
	_ "github.com/go-playground/validator/v10"
)

type User struct {
	IdUser      int64       `json:"id_user"`
	IdDaerah    int64       `json:"id_daerah"`
	Nip         interface{} `json:"nip"`
	NamaUser    interface{} `json:"nama_user"`
	Jabatan     interface{} `json:"jabatan"`
	IdProfil    int64       `json:"id_profil"`
	LoginName   interface{} `json:"login_name"`
	LoginPasswd interface{} `json:"login_passwd"`
	IdLevel     int64       `json:"id_level"`
	AksesUser   interface{} `json:"akses_user"`
	IsLocked    int         `json:"is_locked"`
	IsDeleted   int         `json:"is_deleted"`
	CreatedAt   interface{} `json:"created_at"`
	UpdatedAt   interface{} `json:"updated_at"`
	IsLogin     int         `json:"is_login"`
	LastLogin   interface{} `json:"last_login"`
	LastIpLogin interface{} `json:"last_ip_login"`
	NamaBidang  interface{} `json:"nama_bidang"`
	IdUnik      interface{} `json:"id_unik"`
	TokenLogin  interface{} `json:"token_login"`
}

// FromJSON decode json to user struct
func (u *User) FromJSON(msg []byte) error {
	return json.Unmarshal(msg, u)
}

// ToJSON encode user struct to json
func (u *User) ToJSON() []byte {
	str, _ := json.Marshal(u)
	return str
}
