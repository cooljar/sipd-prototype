package repository

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"golang.org/x/crypto/bcrypt"
	"serviceAuth/models"
	_mErrors "serviceAuth/models/errors"
	"serviceAuth/models/form"
	"time"
)

type UserRepository struct {
	Conn *pgxpool.Pool
}

// NewUserRepository will create an object that represent the user Repository interface
func NewUserRepository(conn *pgxpool.Pool) *UserRepository {
	return &UserRepository{Conn: conn}
}

func (ur *UserRepository) Signup(sf form.SignupForm) (err error) {
	var usernameExists bool

	usernameExists, err = isExistByUsername(ur.Conn, sf.Username)
	if err != nil {
		return
	}
	if usernameExists {
		err = _mErrors.DataValidationError{Field: "username", Message: "username already taken"}
		return
	}

	ts := time.Now()

	user := models.User{
		NamaUser:           sf.Username,
		CreatedAt:          ts.Format(""),
		UpdatedAt:          ts.Format(""),
	}

	qStr := `insert into "user" (id_daerah, nip, nama_user, jabatan, id_profil, login_name, login_passwd, id_level, akses_user, is_locked, is_deleted,
		created_at, updated_at, is_login, last_login, last_ip_login, nama_bidang, id_unik, token_login) 
		VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19) returning id`
	_, err = ur.Conn.Exec(
		context.Background(),
		qStr,
		user.IdDaerah,
		user.Nip,
		user.NamaUser,
		user.Jabatan,
		user.IdProfil,
		user.LoginName,
		user.LoginPasswd,
		user.IdLevel,
		user.AksesUser,
		user.IsLocked,
		user.IsDeleted,
		user.CreatedAt,
		user.UpdatedAt,
		user.IsLogin,
		user.LastLogin,
		user.LastIpLogin,
		user.NamaBidang,
		user.IdUnik,
		user.TokenLogin,
		)
	if err != nil {
		return
	}

	return
}

func (ur *UserRepository) Login(lf form.LoginForm) (u models.User, err error) {
	var isLocked, isDeleted, isLogin  int
	var idUser, idDaerah, idProfil, idLevel int64
	var nip, namaUser, jabatan, loginName, loginPasswd, aksesUser, createdAt, updatedAt, lastLogin, lastIpLogin, namaBidang, idUnik, tokenLogin interface{}

	qStr := `SELECT id_user, id_daerah, nip, nama_user, jabatan, id_profil, login_name, login_passwd, id_level, akses_user, is_locked, is_deleted,
		created_at, updated_at, is_login, last_login, last_ip_login, nama_bidang, id_unik, token_login 
		FROM "auth"."d_user" WHERE login_name = $1`
	err = ur.Conn.QueryRow(context.Background(), qStr, lf.Username).Scan(
		&idUser,
		&idDaerah,
		&nip,
		&namaUser,
		&jabatan,
		&idProfil,
		&loginName,
		&loginPasswd,
		&idLevel,
		&aksesUser,
		&isLocked,
		&isDeleted,
		&createdAt,
		&updatedAt,
		&isLogin,
		&lastLogin,
		&lastIpLogin,
		&namaBidang,
		&idUnik,
		&tokenLogin,
		)
	if err != nil {
		return
	}

	u.IdUser = idUser
	u.IdDaerah = idDaerah
	u.Nip = nip
	u.NamaUser = namaUser
	u.Jabatan = jabatan
	u.IdProfil = idProfil
	u.LoginName = loginName
	u.LoginPasswd = loginPasswd
	u.IdLevel = idLevel
	u.AksesUser = aksesUser
	u.IsLocked = isLocked
	u.IsDeleted = isDeleted
	u.CreatedAt = createdAt
	u.UpdatedAt = updatedAt
	u.IsLogin = isLogin
	u.LastLogin = lastLogin
	u.LastIpLogin = lastIpLogin
	u.NamaBidang = namaBidang
	u.IdUnik = idUnik
	u.TokenLogin = tokenLogin

	return u, nil
}

func isExistByUsername(conn *pgxpool.Pool, username string) (exist bool, err error) {
	qStr := `SELECT EXISTS(SELECT 1 FROM "user" WHERE username = $1)`
	err = conn.QueryRow(context.Background(), qStr, username).Scan(&exist)
	if err != nil {
		return false, err
	}

	return
}

func isExistByEmail(conn *pgxpool.Pool, email string) (exist bool, err error) {
	qStr := `SELECT EXISTS(SELECT 1 FROM "user" WHERE email = $1)`
	err = conn.QueryRow(context.Background(), qStr, email).Scan(&exist)
	if err != nil {
		return false, err
	}

	return
}

func generatePasswordHash(password string) (string, error) {
	bytesPwd, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytesPwd), err
}
