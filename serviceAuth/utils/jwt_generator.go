package utils

import (
	"github.com/golang-jwt/jwt"
	"os"
	"serviceAuth/models"
	"strconv"
	"time"
)

// GenerateNewAccessToken func for generate a new Access token.
func GenerateNewAccessToken(u *models.User) (string, error) {
	// Set secret key from .env file.
	secret := os.Getenv("JWT_SECRET_KEY")

	// Set expires minutes count for secret key from .env file.
	minutesCount, _ := strconv.Atoi(os.Getenv("JWT_SECRET_KEY_EXPIRE_MINUTES"))

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["id_user"] = u.IdUser
	claims["id_daerah"] = u.IdDaerah
	claims["nip"] = u.Nip
	claims["nama_user"] = u.NamaUser
	claims["jabatan"] = u.Jabatan
	claims["id_profil"] = u.IdProfil
	claims["login_name"] = u.LoginName
	claims["login_passwd"] = u.LoginPasswd
	claims["id_level"] = u.IdLevel
	claims["akses_user"] = u.AksesUser
	claims["is_locked"] = u.IsLocked
	claims["is_deleted"] = u.IsDeleted
	claims["created_at"] = u.CreatedAt
	claims["updated_at"] = u.UpdatedAt
	claims["is_login"] = u.IsLogin
	claims["last_login"] = u.LastLogin
	claims["last_ip_login"] = u.LastIpLogin
	claims["nama_bidang"] = u.NamaBidang
	claims["id_unik"] = u.IdUnik
	claims["token_login"] = u.TokenLogin
	claims["exp"] = time.Now().Add(time.Minute * time.Duration(minutesCount)).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(secret))
	if err != nil {
		// Return error, it JWT token generation failed.
		return "", err
	}

	return t, nil
}
