package controller

import (
	"serviceProfile/models"
	"serviceProfile/repository"
	"time"
)

type ProfileController struct {
	Repo *repository.ProfileRepository
	contextTimeout time.Duration
}

func NewProfileController(repo *repository.ProfileRepository, timeout time.Duration) *ProfileController {
	return &ProfileController{
		Repo:           repo,
		contextTimeout: timeout,
	}
}

func (pc *ProfileController) GetProfile(pm models.Profile) (p models.Profile, err error) {
	return pc.Repo.GetProfile(pm)
}

func (pc *ProfileController) UpdateProfile(mp models.Profile) (p models.Profile, err error) {
	return pc.Repo.UpdateProfile(mp)
}
