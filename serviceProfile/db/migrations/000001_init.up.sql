-- Add UUID extension
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Set timezone
-- For more information, please visit:
-- https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
-- SET TIMEZONE="UTC";

-- Create user table
create table "profile"
(
    id UUID DEFAULT uuid_generate_v4 () PRIMARY KEY,
    nip                varchar(255)       not null,
    nik                varchar(25)        not null,
    full_name             varchar(255)       not null,
    created_at           integer  default 0 not null,
    updated_at           integer  default 0 not null
);
