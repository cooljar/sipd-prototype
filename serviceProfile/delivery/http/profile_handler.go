package http

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"serviceProfile/controller"
	"serviceProfile/models"
	"serviceProfile/models/form"
	"serviceProfile/models/http_response"
)

type ProfileHandler struct {
	Controller *controller.ProfileController
	Validate *validator.Validate
}

func NewProfileHandler(r fiber.Router, validator *validator.Validate, controller *controller.ProfileController )  {
	handler := &ProfileHandler{
		Controller: controller,
		Validate:   validator,
	}

	r.Post("/update-profile", handler.UpdateProfile)
	r.Get("/profile", handler.Profile)

	//rUserPrivate := rapiPrivate.Group("/user")
	//rUserPrivate.Get("/refresh-token", handler.RefreshToken)
}

// UpdateProfile func for update profile.
// @Summary user update profile
// @Description Update profile information.
// @Tags Profile
// @Accept mpfd
// @Param nik formData string true "NIK"
// @Param nip formData string true "NIP"
// @Param full_name formData string true "full_name"
// @Produce json
// @Security ApiKeyAuth
// @success 200 {object} models.Profile "Update Profile Success"
// @Failure 400 {object} http_response.HTTPError
// @Failure 404 {object} http_response.HTTPError
// @Failure 422 {array} http_response.HTTPError
// @Failure 500 {object} http_response.HTTPError
// @Router /auth/update-profile [post]
func (uh *ProfileHandler) UpdateProfile(c *fiber.Ctx) error {
	user := c.Locals("jwt").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	modelProfile := models.Profile{
		ID:        claims["id"].(string),
	}

	updateProfileForm := form.ProfileForm{
		Nik:         	c.FormValue("nik"),
		Nip:          c.FormValue("nip"),
		FullName:       c.FormValue("full_name"),
	}

	// Validate form input
	err := uh.Validate.Struct(updateProfileForm)
	if err != nil {
		return http_response.NewHttpError(c, err)
	}

	modelProfile.ID = claims["id"].(string)
	modelProfile.Username = claims["username"].(string)
	modelProfile.Email = claims["email"].(string)
	modelProfile.Phone = claims["phone"].(string)
	modelProfile.FullName = updateProfileForm.FullName
	modelProfile.Nik = updateProfileForm.Nik
	modelProfile.Nip = updateProfileForm.Nik

	modelProfile, err = uh.Controller.UpdateProfile(modelProfile)
	if err != nil {
		return http_response.NewHttpError(c, err)
	}

	return c.JSON(modelProfile)
}

// Profile func for get profile information.
// @Summary user get profile
// @Description get profile information.
// @Tags Profile
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} models.Profile "Profile information"
// @Failure 400 {object} http_response.HTTPError
// @Failure 422 {object} []http_response.HTTPError
// @Failure 404 {object} http_response.HTTPError
// @Failure 500 {object} http_response.HTTPError
// @Router /auth/profile [get]
func (uh *ProfileHandler) Profile(c *fiber.Ctx) error {
	user := c.Locals("jwt").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	modelProfile := models.Profile{
		ID:        claims["id"].(string),
		Username:  claims["username"].(string),
		Email:     claims["email"].(string),
		Phone:     claims["phone"].(string),
	}

	profile, err := uh.Controller.GetProfile(modelProfile)
	if err != nil {
		return http_response.NewHttpError(c, err)
	}

	return c.JSON(profile)
}