package form

type ProfileForm struct {
	Nip      string `json:"nip" validate:"required"`
	Nik      string `json:"nik" validate:"required"`
	FullName string `json:"full_name" validate:"required"`
}
