package models

import (
	"encoding/json"
	_ "github.com/go-playground/validator/v10"
)

type Profile struct {
	ID        string `json:"id"`
	Username  string    `json:"username"`
	Email     string    `json:"email"`
	Phone     string    `json:"phone"`
	Nip       string    `json:"nip"`
	Nik       string    `json:"nik"`
	FullName  string    `json:"full_name"`
	CreatedAt int       `json:"created_at"`
	UpdatedAt int       `json:"updated_at"`
}

// FromJSON decode json to user struct
func (p *Profile) FromJSON(msg []byte) error {
	return json.Unmarshal(msg, p)
}

// ToJSON encode user struct to json
func (p *Profile) ToJSON() []byte {
	str, _ := json.Marshal(p)
	return str
}
