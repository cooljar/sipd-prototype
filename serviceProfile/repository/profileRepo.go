package repository

import (
	"context"
	"fmt"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"serviceProfile/models"
	"time"
)

type ProfileRepository struct {
	Conn *pgxpool.Pool
}

// NewProfileRepository will create an object that represent the user Repository interface
func NewProfileRepository(conn *pgxpool.Pool) *ProfileRepository {
	return &ProfileRepository{Conn: conn}
}

func (pr *ProfileRepository) UpdateProfile(mp models.Profile) (models.Profile, error) {
	var tag pgconn.CommandTag

	now := time.Now().Unix()

	qStr := `INSERT INTO "profile" (id, nip, nik, full_name, created_at, updated_at)
			VALUES($1,$2,$3,$4,$5,$6) 
			ON CONFLICT (id) 
			DO 
   			UPDATE SET nip = EXCLUDED.nip, nik = EXCLUDED.nik, updated_at = EXCLUDED.updated_at;`
	tag, err := pr.Conn.Exec(context.Background(), qStr, mp.ID, mp.Nip, mp.Nik, mp.FullName, now, now)
	if err != nil {
		return mp, err
	}
	fmt.Println("tag: ", tag)

	if mp.CreatedAt == 0 {
		mp.CreatedAt = int(now)
	}

	mp.UpdatedAt = int(now)

	return mp, nil
}

func (pr *ProfileRepository) GetProfile(pm models.Profile) (models.Profile, error) {
	var id, nip, nik, fullName string
	var createdAt, updatedAt int
	qStr := `SELECT id, nip, nik, full_name, created_at, updated_at FROM "profile" WHERE id = $1`
	err := pr.Conn.QueryRow(context.Background(), qStr, pm.ID).Scan(&id, &nip, &nik, &fullName, &createdAt, &updatedAt)
	if err != nil{
		if err == pgx.ErrNoRows {
			err = nil
		}else {
			return pm, err
		}
	}

	pm.ID = id
	pm.Nip = nip
	pm.Nik = nik
	pm.FullName = fullName
	pm.CreatedAt = createdAt
	pm.UpdatedAt = updatedAt

	return pm, nil
}

